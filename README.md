# Talking ben discord bot
![ben](images/ben.png)

Ben the dog is a silly discord bot I made for fun using `discord.py`. Turns out, it's really simple to use.

To use the bot, simply ask ben a question. If your message contains the word `ben` (case insensitive) and a questionmark `?`, the bot will read it as a question to itself. Just like with the real ben, it can reply with 1 of 4 possible options:

![options](images/options.png)

If you want to run the bot, you'll need `discord.py` installed (`pip install -U discord.py`). You'll also need to set the variable TOKEN on `main.py` to your discord bot token. Then simply run `python main.py` and the bot should appear online.
