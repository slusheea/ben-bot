import discord
from discord.ext import commands
from random import choice

TOKEN = "DISCORD_BOT_TOKEN"

intents = discord.Intents.default()
intents.message_content = True

bot = commands.Bot(command_prefix="?", intents=discord.Intents.all(), case_insensitive=True)

options = ["Yes", "No", "Hohoho!", "Eugh!"]
@bot.event
async def on_message(message):
    if "ben" in message.content.lower() and "?" in message.content.lower():
        ben_choice = choice(options)
        if ben_choice == "Yes":
            embed=discord.Embed(title=f"Yes", description=f":dog:") # F-Strings!
            embed.set_thumbnail(url="https://cdn.discordapp.com/attachments/987288550888312842/1064238016845074472/image.png")
            await message.channel.send(embed=embed)
        elif ben_choice == "No":
            embed=discord.Embed(title=f"No", description=f":dog:") # F-Strings!
            embed.set_thumbnail(url="https://cdn.discordapp.com/attachments/987288550888312842/1064237424080867328/image.png")
            await message.channel.send(embed=embed)
        elif ben_choice == "Hohoho!":
            embed=discord.Embed(title=f"Hohoho!", description=f":dog:") # F-Strings!
            embed.set_thumbnail(url="https://cdn.discordapp.com/attachments/987288550888312842/1064238272634687689/image.png")
            await message.channel.send(embed=embed)
        elif ben_choice == "Eugh!":
            embed=discord.Embed(title=f"Eugh!", description=f":dog:") # F-Strings!
            embed.set_thumbnail(url="https://cdn.discordapp.com/attachments/987288550888312842/1064238540000596069/image.png")
            await message.channel.send(embed=embed)

    else:
        await bot.process_commands(message)

bot.run(TOKEN)
